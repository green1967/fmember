import React from 'react';
import { NavLink } from 'react-router-dom';
import '../navigationitems.css';


const navigationItem = ( props ) => (
    // <li className={classes.NavigationItem}>
        <NavLink 
            to={props.link}
            exact={props.exact}
            activeClassName={props.class}>{props.children}</NavLink>
    // </li>
);

export default navigationItem;
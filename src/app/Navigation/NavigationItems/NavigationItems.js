import React, { Fragment} from 'react';
import './navigationitems.css';
import NavigationItem from './NavigationItem/NavigationItem';

const navigationItems = () => (
    <Fragment>
     {/* <ul className={classes.NavigationItems}> */}
    <div className="menu-item">
        <NavigationItem link="/home" class="menu-ico live active menu-title active" exact>
            <div className="menu-ico live"></div>
			<div className="menu-title">Featured</div>
        </NavigationItem>
    </div>
    <div className="menu-item">
        <NavigationItem link="/classes"  class="menu-ico classes active menu-title active" exact>
            <div className="menu-ico classes"></div>
			<div className="menu-title">Classes</div>
        </NavigationItem>
    </div>
    <div className="menu-item">
        <NavigationItem link="/trainers"  class="menu-ico pros active menu-title active" exact>
            <div className="menu-ico pros"></div>
			<div className="menu-title">Trainers</div>
        </NavigationItem>
    </div>
    <div className="menu-item">
        <NavigationItem link="/profile" class="menu-ico profile active menu-title active" exact>
            <div className="menu-ico profile"></div>
			<div className="menu-title">Profile</div>
        </NavigationItem>
    </div>
    <div className="menu-item">    
    <NavigationItem link="/more" class="menu-ico more active menu-title active" exact>
            <div className="menu-ico more"></div>
			<div className="menu-title">More</div>
        </NavigationItem>
    </div>
 {/* </ul> */}
    </Fragment>
);

export default navigationItems;
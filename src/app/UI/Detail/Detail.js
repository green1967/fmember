import React from 'react';
import './detail.css';
import ReactPlayer from 'react-player';


const Detail = (props) => {
    
// console.log('file_name=', 'https://member.fitscope.com/public/wowza/' + props.it.file_name + '_720.mp4');
    return(
                <div className="class-video">
                    <div className="back-btn" onClick={props.onClick}>BACK</div>
                    <div className="list-title">{props.it.title} - {props.it.user_name?props.it.user_name:props.it.pro_name}</div>
    
                        <div className="list-wrap">
                            <div className="list-item-detail">
                                
                            <ReactPlayer url= {props.it.file_name} controls light={props.it.image} width="100%" height="100%"/>                        
    
                        </div>
                            <div className="stream-details">
                                <div className="class-description">{props.it.description}</div>
                            </div>
                        </div>
                </div>
    
            )

} 

export default Detail;

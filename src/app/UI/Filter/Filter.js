import React, {useState} from 'react';
import  './filter.css';
import SlidingPanel from 'react-sliding-side-panel';
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';

const Filter = (props) => {

    const [openPanel, setOpenPanel] = useState(false);
 //   var eq_list='';
    const eq_list = props.equipList.map( item => {
 //       console.log('item=', item);
        // if (item.id === props.activity) {
        //     eq_list = item.equipment.map( itm => {
 //               console.log('itm.equip_name=', itm.equip_name);
                return <div className={(item.id === props.filter.equipment)?"filter-btn active":"filter-btn"} key={item.id} onClick={()=>props.onClick('equipment',item.id)}>
                            {item.name}
                        </div>
        //     })
        // }
    });

    const lev_list = props.levelList.map(item => ( 
        <div className={(item.id === props.filter.level)?"filter-btn active":"filter-btn"} key={item.id} onClick={()=>props.onClick('level',item.id)}>
        {item.name}
         </div>
    ))

    const tr_list = props.trainerList.map(item => ( 
        <div className={(item.id === props.filter.trainer)?"filter-btn active":"filter-btn"} key={item.id} onClick={()=>props.onClick('trainer',item.id)}>
        {item.name}
         </div>
    ))

    const gnr_list = props.genreList.map(item => ( 
        <div className={(item.id === props.filter.genre)?"filter-btn active":"filter-btn"} key={item.id} onClick={()=>props.onClick('genre',item.id)}>
        {item.name}
         </div>
    ))

 //   const tr_list =  

 //   console.log('eq_list=', eq_list);

    return(
    <div>
      <div>
        {/* <button type="button" onClick={() => setOpenPanel(true)}>Open</button> */}
        <div className="filter-line">
				<div className="filter-open-btn" onClick={() => setOpenPanel(true)}>Filters</div>
				<div className="filter-clear-btn" onClick={props.onFilterClearHandler}>Clear</div>
			</div>
      </div>
      <SlidingPanel
        type={'right'}
        isOpen={openPanel}
        backdropClicked={() => setOpenPanel(false)}
        size={30}
        panelClassName="additional-className"
      >
        <div>
        <div className="panel-container">
		<Accordion>
			<Card>
			<div className="row filter-acc-wrap">			
				<Accordion.Toggle as={Card.Header} eventKey="0" className="filter-acc-header">Equipment</Accordion.Toggle>
				<Accordion.Collapse eventKey="0" className="filter-acc-body">
					<div className="btn-filter-wrap">
						
                            {eq_list}
						
					</div>
				</Accordion.Collapse>
			</div>
			</Card>

			<Card>
			<div className="row filter-acc-wrap">
				<Accordion.Toggle as={Card.Header} eventKey="1" className="filter-acc-header">Level</Accordion.Toggle>
				<Accordion.Collapse eventKey="1" className="filter-acc-body">
					<div className="btn-filter-wrap">
						
                            {lev_list}
						
					</div>
				</Accordion.Collapse>
			</div>
			</Card>
	
			<Card>
			<div className="row filter-acc-wrap">
				<Accordion.Toggle as={Card.Header} eventKey="2"  className="filter-acc-header">Duration</Accordion.Toggle>
				<Accordion.Collapse eventKey="2" className="filter-acc-body">
					<div className="btn-filter-wrap">
						<div
							className="filter-btn" onClick={()=>props.onClick('duration',0)}
							>
							15 min or less
						</div>
						<div
							className="filter-btn" onClick={()=>props.onClick('duration',1)}
							>
							16-30 min
						</div>
						<div
							className="filter-btn" onClick={()=>props.onClick('duration',2)}
							>
							More than 30 min
						</div>
					</div>
				</Accordion.Collapse>
			</div>
			</Card>

			<Card>
			<div className="row filter-acc-wrap" v-if="trai">
				<Accordion.Toggle as={Card.Header} eventKey="3" className="filter-acc-header">Trainer</Accordion.Toggle>
				<Accordion.Collapse eventKey="3"  className="filter-acc-body">
					<div className="btn-filter-wrap">
						
                            {tr_list}
						
					</div>
				</Accordion.Collapse>
			</div>
			</Card>
			
			<Card>
			<div className="row filter-acc-wrap">
				<Accordion.Toggle as={Card.Header} eventKey="4" className="filter-acc-header">Music Genre</Accordion.Toggle>
				<Accordion.Collapse eventKey="4" className="filter-acc-body">
					<div className="btn-filter-wrap">
						
                            {gnr_list}
						
					</div>
				</Accordion.Collapse>
			</div>
			</Card>
			</Accordion>
        </div>		
        </div>		
      </SlidingPanel>
    </div>
        // <div className={style.sidebaPanel}>

		// 	<div className={style.rowFilterAccWrap}>
		// 		<button className={style.filterAccHeader}>Equipment</button>
		// 		<div className={style.filterAccBody}>
		// 			<div className={style.btnFilterWrap}>
		// 				<div className={style.filterBtn}>
		// 					{props.equip_name}
		// 				</div>
		// 			</div>
		// 		</div>
        //     </div>
        // </div>

		// 	<!-- LEVEL LIST -->
		// 	<div className="row filter-acc-wrap">
		// 		<b-btn v-b-toggle="'level-wrap'" className="filter-acc-header">Level</b-btn>
		// 		<b-collapse id="level-wrap" className="filter-acc-body">
		// 			<div className="btn-filter-wrap">
		// 				<div v-for="ll in levelList"
		// 					:key="ll.id"
		// 					className="filter-btn"
		// 					:className="{'active' : ll.id == currLevel }"
		// 					@click="currLevel = (currLevel == ll.id) ? 0 : ll.id ">
		// 					{{ll.name}}
		// 				</div>
		// 			</div>
		// 		</b-collapse>
		// 	</div>

		// 	<!-- DURATION LIST -->
		// 	<div className="row filter-acc-wrap">
		// 		<b-btn v-b-toggle="'duration-wrap'" className="filter-acc-header">Duration</b-btn>
		// 		<b-collapse id="duration-wrap" className="filter-acc-body">
		// 			<div className="btn-filter-wrap">
		// 				<div
		// 					className="filter-btn"
		// 					:className="{'active' : currDuration == 1 }"
		// 					@click="currDuration = (currDuration == 1) ? 0 : 1">
		// 					15 min or less
		// 				</div>
		// 				<div
		// 					className="filter-btn"
		// 					:className="{'active' : currDuration == 2 }"
		// 					@click="currDuration = (currDuration == 2) ? 0 : 2">
		// 					16-30 min
		// 				</div>
		// 				<div
		// 					className="filter-btn"
		// 					:className="{'active' : currDuration == 3 }"
		// 					@click="currDuration = (currDuration == 3) ? 0 : 3">
		// 					More than 30 min
		// 				</div>
		// 			</div>
		// 		</b-collapse>
		// 	</div>

		// 	<!-- TRAINER LIST -->
		// 	<div className="row filter-acc-wrap" v-if="trainerList">
		// 		<b-btn v-b-toggle="'trainer-wrap'" className="filter-acc-header">Trainer</b-btn>
		// 		<b-collapse id="trainer-wrap" className="filter-acc-body">
		// 			<div className="btn-filter-wrap">
		// 				<div v-for="tl in trainerList"
		// 					:key="tl.user_id"
		// 					className="filter-btn"
		// 					:className="{'active' : tl.user_name == currTrainer}"
		// 					@click="setTrainer(tl)">
		// 					{{tl.user_name}}
		// 				</div>
		// 			</div>
		// 		</b-collapse>
		// 	</div>

		// 	<!-- MUSIC GENRE LIST -->
		// 	<div className="row filter-acc-wrap">
		// 		<b-btn v-b-toggle="'music-wrap'" className="filter-acc-header">Music Genre</b-btn>
		// 		<b-collapse id="music-wrap" className="filter-acc-body">
		// 			<div className="btn-filter-wrap">
		// 				<div v-for="ml in musicList"
		// 					:key="ml.id"
		// 					className="filter-btn"
		// 					:className="{'active' : ml.id == currMusic }"
		// 					@click="currMusic = (currMusic == ml.id) ? 0 : ml.id ">
		// 					{{ml.music_genre}}
		// 				</div>
		// 			</div>
		// 		</b-collapse>
		// 	</div>

		// </div>


    )

}

export default Filter;
import React from 'react';
import './class.css';
import moment from 'moment-timezone';

//className={ props.it.is_live == 1 ? style.streamIsLiveOnline : '' + props.is_viwe == 1 ?style.streamIsLiveWatched : '' + props.is_viwe == 0? style.streamIsLiveUnwatched : '' }
const Class = (props) => {
//    console.log(props.it.image);

const  getStartTimeByZone =  sDate => {
        // return moment(sDate.full_plan_date, 'YYYY-MM-DD HH:mm A Z').tz( moment.tz.guess() ).format('dddd, MMMM DD, h:mm A');
        return moment(sDate, 'YYYY-MM-DD HH:mm A Z').tz( moment.tz.guess() ).format('dddd, MMMM DD');
    }

return (

    <div className="class-wrap" onClick={props.onClick}>
        <div className="stream-image" style={{'backgroundImage' : 'url(' + props.it.image + ')' }}></div>       
        <div className="stream-class-info">
            <div className="header-line">
                <div className= {props.it.is_viwe === 1 ? "stream-is-live watched" : "stream-is-live unwatched"} >
                    { props.it.is_live === 1 ? 'Online' : (props.it.is_viwe === 1 ? 'Watched' : 'Unwatched') }
                </div>
                <div className="stream-viwers">{props.it.viwers_cnt} viewers</div>
            </div>
            <div className="content-line">
                <div className="class-title-big">{props.it.title} - {props.it.activity_name}</div>
                <div className="class-title">{props.it.user_name}</div>
                <div className="class-title">{getStartTimeByZone(props.it.start_date)}</div>
            </div>
        </div>
    </div>    
)


}

export default Class;
import React, { Component } from 'react';
import './classes.css';
import { connect } from 'react-redux';
import Class from './Class';
import Icon from './Icon';
import Detail from '../UI/Detail/Detail';
import Filter from '../UI/Filter/Filter';
import {axios_i} from '../utils/utility';
import axios from 'axios';
import Spinner from '../UI/Spinner/Spinner';


class Classes  extends Component {

    state = {
        videoList:[],
        activityList:[],
        equipList:[],
        levelList:[],        
        genreList:[],
        array4Filter: {
            trianerArray:[],
            equipArray:[],
            durationArray:[],
            genreArray:[],
            levelArray:[],
        },
        filter: {
            activity:'',
            equipment: '',
            level:'',
            duration:'',
            trainer:'',
            genre: '',
        },
        details: {},
        flag: false,
        showDetails: false,
        loading: false,
    }

    componentDidMount () {

        const getStreamList = () => axios_i.get('/streams/', {headers: {
                'Authorization': 'Bearer ' + this.props.token}});
        

        const getActivityList = () => {
            return axios_i.get('/activities/', {headers: {
                'Authorization': 'Bearer ' + this.props.token}});
          }
        
        const getEquipList = () => {
            return axios_i.get('/equips/', {headers: {
                'Authorization': 'Bearer ' + this.props.token}});
          }

        const getLevelList = () => {
            return axios_i.get('/levels/', {headers: {
                'Authorization': 'Bearer ' + this.props.token}});
          }

        const getGenreList = () => {
            return axios_i.get('/genres/', {headers: {
                'Authorization': 'Bearer ' + this.props.token}});
          }

        this.setState({loading:true});
        axios.all([getStreamList(), getActivityList(), getEquipList(), getLevelList(), getGenreList()])
          .then(axios.spread((streams, activities, equips, levels, genres) => {
            this.setState({videoList: streams.data, activityList: activities.data, equipList: equips.data, levelList:levels.data, genreList: genres.data, loading: false});
          }))
          .catch( error => {
            this.setState({loading: false});
            console.log(error);                
            } );
        

        
 //       this.props.featuredFetch(this.props.user_id, this.props.token);
    }

    iconOnCickHandle = (aID) => {
        this.setState(prevState => ({
            filter: {                   
                ...prevState.filter,    
                activity: aID
            }
        }));
        this.setState({flag: true});

            var trList=[];
            var eqList=[];
            var lvList=[];
            var gnList=[];
            var myTrSet = new Set();
            var myEqSet = new Set();
            var myLvSet = new Set();
            var myGnSet = new Set();
            this.state.videoList.map( item => {
                
                if(item.activity_id === aID) {

                    if (!myTrSet.has(item.user_id)) { trList.push({id: item.user_id, name: item.user_name}); myTrSet.add(item.user_id)}
                    item.equip_ids.map( id => {
                         if(!myEqSet.has(id)) {
                            myEqSet.add(id);
                            this.state.equipList.map( itm => {
                                if(itm._id === id) eqList.push({id: itm._id, name: itm.name});
                                return null;
                             })
                        }
                        return null;
                    })
                    if(!myLvSet.has(item.level_id)) {
                        myLvSet.add(item.level_id);
                        this.state.levelList.map( itm => {
                            if(itm.id === item.level_id) lvList.push({id:itm.id, name: itm.name});
                            return null;
                        })
                    }
                    if(!myGnSet.has(item.music_genre_id)) {
                        myGnSet.add(item.music_genre_id);
                        this.state.genreList.map( i => {
                            if(i.id === item.music_genre_id) gnList.push({id: i.id, name: i.music_genre});
                            return null;
                        })
                    }
                }
                return null;
            });
            this.setState(prevState => ({
                array4Filter: {                   
                    ...prevState.array4Filter,    
                    trianerArray: trList,
                    equipArray: eqList,
                    levelArray: lvList,
                    genreArray: gnList,
                }
            }));
 
    }

    onClickHandle = (cur_v) => {        
                    this.setState({showDetails: true, details: cur_v});  
                }  
    onBackHandle = () => {
         this.setState({showDetails: false});
        }

    onListBackHandle = () => {
            this.setState({flag: false});
            this.onFilterClearHandler();
            }
    
    onFilterClearHandler = () => {
        this.setState(prevState => ({
            filter: {                   
                ...prevState.filter,    
                level: '',
                trainer: '',
                equipment: '',
                genre: '',
                duration: '',
            }
        }));
    }

    onFilterHandler = (type, id) => {
        switch(type) {
        case 'level':
            this.setState(prevState => ({
            filter: {                   
                ...prevState.filter,    
                level: id === prevState.filter.level?'':id
            }
        }));
        break;
        case 'equipment':
            this.setState(prevState => ({
            filter: {                   
                ...prevState.filter,    
                equipment: id === prevState.filter.equipment?'':id
            }
        }));
        break;
        case 'trainer':
            this.setState(prevState => ({
            filter: {                   
                ...prevState.filter,    
                trainer: id === prevState.filter.trainer?'':id
            }
        }));
        break;
        case 'genre':
            this.setState(prevState => ({
            filter: {                   
                ...prevState.filter,    
                genre: id === prevState.filter.genre?'':id
            }
        }));
        break;
        case 'duration':
            this.setState(prevState => ({
            filter: {                   
                ...prevState.filter,    
                duration: id === prevState.filter.duration?'':id
            }
        }));
        break;
        default:
        console.log("NO ACTIVITY FOUND");
    }
    }
    
    getClassesByFilter =() => {
            var cnt=0;
            var res=[];  
                Object.values(this.state.filter).map( (item, index) => {
                switch(index) {
                case 0: 
                    if (!(item === '')) res = this.state.videoList.map( itm => {
                     if(item === itm.activity_id) return itm; else return null;
                })
                break;
                case 1: 
                    if(!(item === '')) res = res.map( itm => {
                    if (itm) {
                     if(!(itm.equip_ids.indexOf(item) === -1)) return  itm;
                }
                 return null;    
                })
                break;
                case 2:
                    if(!(item === '')) res = res.map( itm => {
                        if (itm) {
                        if(item === itm.level_id) return  itm; else return null;
                } else return null;})    
                break;
                case 3:
                    if(!(item === '')) res = res.map( itm => {
                        if (itm) {
                        switch(item) {
                            case 0:
                                if(parseInt(itm.duration) <= 15) return  itm;
                            break;
                            case 1:
                                if(parseInt(itm.duration) > 15 && parseInt(itm.duration) <= 30) return  itm;
                            break;
                            case 2:
                                if(parseInt(itm.duration) > 30) return  itm;
                            break;
                            default:
                                return null;
                        }
                    }
                    return null;
                })
                break;
                case 4:
                    if(!(item === '')) res = res.map( itm => {
                        if (itm) {
                        if(item === itm.user_id) return  itm;
                } return null;})  
                break;
                case 5:
                    if(!(item === '')) res = res.map( itm => {
                        if (itm) {
                        if(item === itm.music_genre_id) return  itm;
                } return null;}) 
                break;
                default:
                    return null;
            }
      
            // return this.state.videoList.map( item => {
            // if(item.activity_id === this.state.filter.activity) return <Class it={item} onClick={()=>this.onClickHandle(item)} />
     return null;       
            
    })
return res.map(item =>{if(item) return <Class it={item} key={cnt++} onClick={()=>this.onClickHandle(item)} />; else return null;});
}



   
    render()
    {
        var cnt=0;
        const show_a = <div className="activity-wrap" >
            {this.state.activityList.map( item => {
                    return <Icon ic={item} key={cnt++} onClick={()=>this.iconOnCickHandle(item.id)}/>
                            
                })}
                </div>
            
         
        
     const a_l = this.getClassesByFilter();
        const show_l = (<div><div className="back-btn" onClick={this.onListBackHandle}>BACK</div><div><Filter onClick={this.onFilterHandler} onFilterClearHandler={this.onFilterClearHandler} activity={this.state.filter.activity} equipList={this.state.array4Filter.equipArray} levelList={this.state.array4Filter.levelArray} trainerList={this.state.array4Filter.trianerArray} genreList = {this.state.array4Filter.genreArray} filter={this.state.filter} /></div><div>{a_l}</div></div>);

        const list = this.state.flag ? show_l : show_a;
        const showDetails = this.state.details ? <Detail it={this.state.details} onClick={this.onBackHandle}/>:null;
        const show = this.state.showDetails ? showDetails : list;



        return (
            <div className="container content-wrap">
            {this.state.loading ? <Spinner/> : show } 
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        user_id: state.user.user,
        token: state.user.token
    };
};

export default connect(mapStateToProps)(Classes);


import React, { Component } from 'react';
import './home.css';
import { connect } from 'react-redux';
import {axios_i} from '../utils/utility';
import moment from 'moment-timezone';
import Detail from '../UI/Detail/Detail';
import Spinner from '../UI/Spinner/Spinner';
import 'pure-react-carousel/dist/react-carousel.es.css';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';


class Home extends Component {

    state={
        featured: {},
        v_data: [],
        details: {},
        video_quality: '720p',            
        show_details: false,
        loading: false,
    };

    getStartTimeByZone =  sDate => {
        // return moment(sDate.full_plan_date, 'YYYY-MM-DD HH:mm A Z').tz( moment.tz.guess() ).format('dddd, MMMM DD, h:mm A');
        return moment(sDate, 'YYYY-MM-DD HH:mm A Z').tz( moment.tz.guess() ).format('dddd, MMMM DD');
    }

    fetchData = () =>{
        
    }
    componentDidMount () {
        this.setState({loading: true});
        axios_i.get('streams/get-feature-stream', {headers: {
        'Authorization': 'Bearer ' + this.props.token}})
            .then( response => {
            if(response.status === 200)  {
                this.setState({featured:response.data});
  //               if(data.status==='OK') {
                }                    
//        });             
        } )
        .catch( error => {
            console.log(error);                
        } );

        axios_i.get('streams/get-gym-stream-home', {headers: {
            'Authorization': 'Bearer ' + this.props.token}})
                .then( response => {
                if(response.status === 200)  {
    //                vid_data = response.data;
                    this.setState({v_data: response.data, loading: false});
     //               console.log('response.home=', response.data);
     //               if(data.status==='OK') {
                    }                    
    //        });             
            } )
            .catch( error => {
                this.setState({loading: false});
                console.log(error);                
            } );

 //           console.log('reposnse.feature=', f_data);
 //           console.log('reposnse.home=', vid_data);
         
  
    }
    

    onClickHandle = (cur_v) => {
//          console.log('https://member.fitscope.com/public/wowza/' + v_data.file_name + '_'+v_data.video_quality + '.mp4');
          this.setState({show_details: true, details: cur_v});  
        }

    
    onBackHandle = () => {
        this.setState({show_details: false});
    }


    render()
    {
        let cnt=0;
    const show_c= this.state.v_data.map(item => (
        <CarouselProvider
        visibleSlides={2}
        totalSlides={item.videos.length}
        step={2}
        naturalSlideWidth={100}
        naturalSlideHeight={60}
        key={cnt++}
        >
    <div className="activity-name">{item.type_name}</div>
        <ButtonBack className="arrow-back"></ButtonBack>
        <Slider>
        {item.videos.map(it => (            
                <Slide key={cnt++}>
                <div className="list-item demand" style={{'backgroundImage' : 'url(' + it.image + ')'}} onClick={()=>this.onClickHandle(it)}>
                <div className="header-line">
                 <div className="stream-is-live-s">
                 { it.is_live === 1 ? 'Online' : (it.is_viwe === 1 ? 'Watched' : 'Unwatched') }
                 </div>
                 <div className="stream-viwers">{it.viwers_cnt} viewers</div>
             </div>

            
             <div className="stream-info">
             <div className="class-pro-name">{it.pro_name}</div>
                 <div className="class-title">{it.title} - {it.activity_name}</div>
                 <div className="class-date">{this.getStartTimeByZone(it.start_date)}</div>	
             </div>
             </div>           
        </Slide>
        ))}
        </Slider> 
        
        <ButtonNext className="arrow-next"></ButtonNext>       
        </CarouselProvider>
    ));
    
        const featured = !this.state.show_details? (
        <div>

            <div className="list-title">Featured</div>
     
            <div className="list-item live"
                 style={{'backgroundImage' : 'url(' + this.state.featured.image + ')'}} onClick={()=>this.onClickHandle(this.state.featured)}>
    
                <div className="stream-info">
                    <div className="class-pro-name">{this.state.featured.pro_name}</div>
                    <div className="class-title">{this.state.featured.title} - {this.state.featured.activity_name}</div>
                    <div className="class-date">{this.getStartTimeByZone(this.state.featured.full_plan_date)}</div>	
                </div>
            </div>
 
        <br></br>
        <br></br>
        {show_c}
        </div>
        ) : (  <Detail it={this.state.details} onClick={this.onBackHandle}/> )
        ;



        return (
            <div className="container content-wrap">
               
                {this.state.loading ? <Spinner /> : featured}                
                {/* {featured} */}
                {/* {this.props.loggingIn && <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" alt='' />} */}
            </div>
        );
    }
}



const mapStateToProps = state => {
    return {
        user_id: state.user.user,
        token: state.user.token
    };
};

export default connect(mapStateToProps)(Home);
import types from './types';
import { store } from 'react-notifications-component';
import {axios_i} from '../../utils/utility';

const featuredStart = () => ({    
        type: types.FEATURED_START
});

const featuredFail = ( error ) => ({
        type: types.FEATURED_FAIL,
        error: error
});

const featuredSuccess = () => ({
    type: types.FEATURED_SUCCESS,
});

const featuredRequest = ( user_id, token ) => {
    return dispatch => {
//        const axios = require('axios');
        dispatch(featuredStart() );
        console.log(user_id,"  ", token);
        axios_i.get('streams/get-feature-stream', {headers: {
            'Authorization': 'Bearer ' + token}})
                .then( response => {
                if(response.status === 200)  {
                    const data = response.data;
                    console.log('reposnse=', data);
     //               if(data.status==='OK') {
                    dispatch( featuredSuccess()); 
                    } else {
                        let error_msg = 'Somthing goes wrong!';
                        if (response.status === 401) {
                            error_msg = 'Invalid Token';
                        };
                        store.addNotification({
                            title: "Erorr!",
                            message: error_msg,
                            type: "danger",
                            insert: "top",
                            container: "top-right",
                            animationIn: ["animated", "fadeIn"],
                            animationOut: ["animated", "fadeOut"],
                            dismiss: {
                              duration: 3000,
                            //   onScreen: true
                            }
                          });
                        dispatch( featuredFail( response.data.message ));
                   }                    
    //        });             
            } )
            .catch( error => {
                console.log(error);                
            } );
    };
//     return dispatch => {
//         dispatch( featuredStart() );
//         let data = new FormData();
//         data.append('user_id', user_id);
//         console.log('user_id=', user_id, 'token=', token);
//         fetch(`http://b.fitscope.com:3000`,
//         {
//             method: 'POST',
//             headers: {
//                 'Authorization': 'Bearer ' + token
//             },
//             body: data
//              })
//             .then( response => {
//                 response.text().then(text=> {
//                     console.log('home_response=', response);
//                     const data = text && JSON.parse(text);
//                     console.log('featured.reposnse=', data);
//                     if(data.status==='OK') {
//   //                   console.log('action_cookie=', cookie.load('token'));
//                     dispatch( featuredSuccess());
//                     } else {
//                         let error_msg = 'Somthing goes wrong! Error = ' + data.content.code;
//                         store.addNotification({
//                             title: "Erorr!",
//                             message: error_msg,
//                             type: "danger",
//                             insert: "top",
//                             container: "top-right",
//                             animationIn: ["animated", "fadeIn"],
//                             animationOut: ["animated", "fadeOut"],
//                             dismiss: {
//                               duration: 3000,
//                             //   onScreen: true
//                             }
//                           });
//                         dispatch( featuredFail( data.content.message ) );
//                     }
                    
//             });             
//             } )
//             .catch( error => {
//                 console.log(error);                
//             } );
//     };
};




export default {
    featuredStart,
    featuredSuccess,
    featuredFail,
    featuredRequest
}
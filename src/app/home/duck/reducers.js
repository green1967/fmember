import types from './types';
import { updateObject } from '../../utils/utility';

const initialState = {
    loading: false
};


const featuredStart = ( state, action ) => {
    return updateObject( state, { loading: true } );
};

const featuredFail = ( state, action ) => {
    return updateObject( state, { loading: false } );
};

const featuredSuccess = ( state, action ) => {
//    console.log('action.token=', action.token);
    return updateObject( state, {
        loading: false
       } );
};


const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case types.FEATURED_START: return featuredStart( state, action );
        case types.FEATURED_SUCCESS: return featuredSuccess( state, action );
        case types.FEATURED_FAIL: return featuredFail( state, action )
        default: return state;
    }
};

export default reducer;
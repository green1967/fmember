const FEATURED_START = 'app/home/duck/FEATURED_START';
const FEATURED_SUCCESS = 'app/login/duck/FEATURED_SUCESS';
const FEATURED_FAIL = 'app/login/duck/FEATURED_FAIL';

export default {
    FEATURED_START,
    FEATURED_SUCCESS,
    FEATURED_FAIL}
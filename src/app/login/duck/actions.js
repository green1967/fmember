import types from './types';
import {axios_i} from '../../utils/utility';
import cookie from 'react-cookies';
import { store } from 'react-notifications-component';
import md5 from 'md5';

const loginStart = () => ({    
        type: types.LOGIN_START
});

const loginFail = ( error ) => ({
        type: types.LOGIN_FAIL,
        error: error
});

const loginSuccess = ( user, token ) => ({
    type: types.LOGIN_SUCCESS,
    user: user,
    token: token
});

const loginRequest = ( username, password ) => {
    const expires = new Date();
    expires.setDate(Date.now() + 180000*60);

    return dispatch => {
 //       const axios = require('axios');
        dispatch( loginStart() );
        let data = {
            email: username,
            password: password,
        };
    //    data.append('email', username);
    //    data.append('password', password);
    //     console.log(username, password);
        axios_i.post('users/login', {email: username, password: md5(password)})
        // fetch(`http://192.168.1.108:4000/users/login`,
        // {
        //     method: 'POST',
        //     headers: {
        //        // 'Content-Type': 'application/json'
        //      'Content-Type': 'application/x-www-form-urlencoded',
        //       },
        //     body: JSON.stringify(data),
        //      })
            .then( response => {
  //              console.log('reposnse=', response.status);
                if(response.status === 200)  {
                    const data = response.data;                    
     //               if(data.status==='OK') {
                    localStorage.setItem('user', JSON.stringify(data)); 
                    cookie.save('token', data.token, { path: '/', expires, maxAge: 18000});
    //                console.log('action_user_id=', data._id) ;
                    dispatch( loginSuccess(data._id, data.token)); 
                                      
    //        });             
            } 
        })
            .catch( error => {
                let error_msg = error.response.data.message;
                            store.addNotification({
                            title: "Erorr!",
                            message: error_msg,
                            type: "danger",
                            insert: "top",
                            container: "top-right",
                            animationIn: ["animated", "fadeIn"],
                            animationOut: ["animated", "fadeOut"],
                            dismiss: {
                              duration: 3000,
                            //   onScreen: true
                            }
                          });
                        logout();
                        dispatch( loginFail( error.response.data.message ) );
                console.log(error.response.status);                
            } );
    };
};

const logout = () => {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
 //   console.log('logout.getlocalstorage=', localStorage.getItem('user'));
}


export default {
    loginStart,
    loginSuccess,
    loginFail,
    loginRequest,
    logout
}
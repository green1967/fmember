import types from './types';
import { updateObject } from '../../utils/utility';

const initialState = {
    user: null,
    token: null,
    loading: false,
    expiried: false
};


const loginStart = ( state, action ) => {
    return updateObject( state, { loading: true } );
};

const loginFail = ( state, action ) => {
    return updateObject( state, { loading: false } );
};

const loginSuccess = ( state, action ) => {
//    console.log('action.token=', action.token);
    return updateObject( state, {
        user: action.user,
        token: action.token,
        loading: false,
        expired: false
    } );
};


const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case types.LOGIN_START: return loginStart( state, action );
        case types.LOGIN_SUCCESS: return loginSuccess( state, action );
        case types.LOGIN_FAIL: return loginFail( state, action )
        default: return state;
    }
};

export default reducer;
import React from 'react';
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';


const faq = () => (
    <div class="page-wrap">

	<div class="container content-wrap">
	
		<div class="contact-page">
			
			<div class="list-title datails-page">FAQ</div>

			<div class="page-content content-header">
				<h3>Billing</h3>
            <Accordion>
			<Card>
			<div className="row filter-acc-wrap">			
				<Accordion.Toggle as={Card.Header} eventKey="0" className="filter-acc-header">How much does Fitscope cost?</Accordion.Toggle>
				<Accordion.Collapse eventKey="0" className="filter-acc-body">
					<div className="btn-filter-wrap">
					<p class="card-text">Fitscope costs USD 4.99/month, and is billed through the platform in which you originated your subscription (iTunes or Stripe payment processing for Android & fitscope.com).  In countries outside of the US, you will be billed the equivalent of USD 4.99/month in your local currency (converted at applicable exchange rate of your platform).</p>				
					</div>
				</Accordion.Collapse>
			</div>
			</Card>

			<Card>
			<div className="row filter-acc-wrap">
				<Accordion.Toggle as={Card.Header} eventKey="1" className="filter-acc-header">How does the 7 day free trial work?</Accordion.Toggle>
				<Accordion.Collapse eventKey="1" className="filter-acc-body">
					<div className="btn-filter-wrap">
                    <p class="card-text">When you subscribe to Fitscope, you have 7 days to cancel before you will be charged. At the end of the free trial period, if you have not cancelled your credit card will be charged automatically.</p>
					</div>
				</Accordion.Collapse>
			</div>
			</Card>
	
			<Card>
			<div className="row filter-acc-wrap">
				<Accordion.Toggle as={Card.Header} eventKey="2"  className="filter-acc-header">How do I use a Promo Code?</Accordion.Toggle>
				<Accordion.Collapse eventKey="2" className="filter-acc-body">
					<div className="btn-filter-wrap">
					<p class="card-text">After you have signed up and registered, you have the opportunity to enter the Promo Code at the Fitscope.com website.  Since you are given an automatic free trial at signup, it does not cost anything to signup and register.  Please sign in to your account at the Fitscope.com website (not the app), select ***more > Subscriptions.  On the Subscriptions page, under the Subscribe Now button, there is a box to enter the promo code.  Once entered, select Apply Code and your promo code free trial period will begin.  Note if you are within the automatic 14-day free trial, once you enter your promo code, the 14-day trial will end and the promo code trial will begin. Besides the 14-day free trial, users are allowed to use only 1 promo code.</p>
					</div>
				</Accordion.Collapse>
			</div>
			</Card>

			<Card>
			<div className="row filter-acc-wrap" v-if="trai">
				<Accordion.Toggle as={Card.Header} eventKey="3" className="filter-acc-header">How do I cancel?</Accordion.Toggle>
				<Accordion.Collapse eventKey="3"  className="filter-acc-body">
					<div className="btn-filter-wrap">
					<p class="card-text">Your Fitscope subscription automatically renews unless auto-renew is turned off at least 24 hours prior to the end of the current period. Your credit card will be charged the monthly subscription rate for renewal within 24 hours prior to the end of the current period. Auto-renew may be turned off in your Account Settings on the platform in which you originated your subscription (iTunes,  or the Fitscope.com website) after purchase. For the Fitscope app, go to ***more > Subscriptions > Manage Existing Subscriptions, and select the Cancel Subscription button. No cancellation of the current subscription is allowed during an active subscription period. A user that cancels during a subscription month will not be charged for the following month.</p>
					</div>
				</Accordion.Collapse>
			</div>
			</Card>			
			</Accordion>
            </div>
        </div>
    </div>
    <script>{window.scrollBy(0, -window.innerHeight)}</script>
    </div>    
);

export default faq;
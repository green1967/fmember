import React from 'react';
import './more.css';
import { Link} from 'react-router-dom';
import cookie from 'react-cookies';


const more = () => {

 const logout = () => {
        cookie.remove('token');
        window.location = "/login";;
    }


return (
    <div className="page-wrap">
    <div className="container content-wrap">
        <div className="col-11 col-lg-10 profile-links-wrap">
            <div className="section-title">ACCOUNT</div>
            <Link key='1' to="#" className="profile-link alerts">Alerts</Link>
           <Link key='2' to="#" className="profile-link subs">Subscriptions</Link>
            <Link key='3' to="#" className="profile-link following">Following</Link>
            <Link key='4' to="#" className="profile-link settings">Settings</Link>
            <div onClick={logout} className="profile-link logout">Logout</div>
           
           <div className="section-title second">LEARN MORE</div>
            <Link key='5'  to="/more/faq" className="profile-link faq">FAQ</Link>
            <Link key='6'  to="#" className="profile-link about">About Us</Link>				
        </div>
    </div>
    </div>
 );
};

export default more;
import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import {Form, Button} from 'react-bootstrap';
import {axios_i} from '../utils/utility';
import { store } from 'react-notifications-component';
import Spinner from '../UI/Spinner/Spinner';
//import Autocomplete from 'react-google-autocomplete';
import './profile.css';

class Profile extends Component {
    state = {
        userInfo: {},
        loading: false,
    }

    componentDidMount() {
      this.setState({loading: true});
      axios_i.get('users/current', {headers: {'Authorization': 'Bearer ' + this.props.token}})
            .then( response => {
            if(response.status === 200)  {
  //            console.log('didmount.response=', response.data);
               this.setState({userInfo : response.data, loading: false});
  //               if(data.status==='OK') {
                }                    
//        });             
        } )
        .catch( error => {
            this.setState({loading: false});
            console.log(error);                
        } );     

     }

     handleChangeName = (e) => {
      let { value } = e.target;
 //     console.log('name=', name, ' value=', value);
      this.setState(prevState=>
        ({
          userInfo: {                   
              ...prevState.userInfo,    
              name: value
          }
      }));
    }


  handleSubmit = (e) => {
    e.preventDefault();

    const { id, name} = this.state.userInfo;
    let formData = new FormData(document.forms.profile);
//        const { dispatch } = this.props;
    if (id && name) {
 //       console.log('users/'+id, "name= ", name);
        this.setState({loading: true});
        axios_i.put('users/'+id, formData,{headers: {'Authorization': 'Bearer ' + this.props.token}})
              .then( response => {
                if(response.status === 200)  {
                  console.log('profile.put=', response.data);
                            store.addNotification({
                            title: "Done!",
                            message: "Profile has been updated suscessfuly!",
                            type: "success",
                            insert: "top",
                            container: "top-right",
                            animationIn: ["animated", "fadeIn"],
                            animationOut: ["animated", "fadeOut"],
                            dismiss: {
                              duration: 3000,
                            //   onScreen: true
                            }
                          }); 
                }
                axios_i.get('users/current', {headers: {'Authorization': 'Bearer ' + this.props.token}})
                .then( response => {
                if(response.status === 200)  {
   //               console.log('submit.response=', response.data);
                   this.setState({userInfo : response.data, loading: false});
      //               if(data.status==='OK') {
                    }                    
    //        });             
                } )
                .catch( error => {
                    this.setState({loading: false});
                    console.log(error);                
                } );                       
            })
            .catch( error => {
                this.setState({loading: false});
                console.log(error);                
            } );

               
    }
}


    render() {
      const profile_form = (<div className="container content-wrap">
      <div className="list-title datails-page">Edit Profile</div>
          <div className="pro-photo" style={{'backgroundImage' : 'url(http://member.gbox.pp.ua:4000/'  + this.state.userInfo.photo + ')'}}></div>
          <Form onSubmit={this.handleSubmit} name="profile" encType="multipart/form-data">
          <Form.Group controlId="photo">
            <Form.Label></Form.Label>
            <Form.Control type="file" placeholder="Select file" name="avatar"/>              
          </Form.Group>          
          <Form.Group controlId="UserName">
            <Form.Label></Form.Label>
            <Form.Control requred type="input" placeholder="Name" name = "name" value={this.state.userInfo.name} onChange={this.handleChangeName}/>
          </Form.Group> 
          <Form.Group controlId="email">
            <Form.Label></Form.Label>
            <Form.Control plaintext readOnly defaultValue={this.state.userInfo.email}/>
          </Form.Group>
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>  
      </div>)

        return (
          <Fragment>
          {this.state.loading ? <Spinner/> : profile_form}  
          </Fragment>
        )
    }

}

const mapStateToProps = state => {
  return {
      user_id: state.user.user,
      token: state.user.token
  };
};

export default connect(mapStateToProps)(Profile);

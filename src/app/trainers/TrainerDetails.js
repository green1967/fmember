import React, {useState} from 'react';
import 'pure-react-carousel/dist/react-carousel.es.css';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import './TrainerInfo.css';
import Detail from '../UI/Detail/Detail';


const TrainerDetails = (props) => {
    const [showDetails, setShowDetails] = useState(false);
    const [item, setItem] = useState(0);
    

    const onClickHandle = (it) => {
        setItem(it);
        setShowDetails(true);
    };

    const onBackHandle = () => {
        setItem(0);
        setShowDetails(false);
    }
 //   console.log('trainers.details=', props.it.name);
    var res = ''; var cnt=0;
    if(!showDetails) res = (<div className="classlist">
    <div className="links-line">
        <div className="back-btn" onClick={props.onBackClick}>BACK</div>
        
        {/* <div classNameName="set-follow-btn">Follow Pro</div> */}
        {/* <div className="unset-follow-btn" @click="unsetFollow()" v-if="props.it.is_follower == 1">Don't Follow</div> */}
    </div>

    <div className="profile-wrap">
        <div className="pro-photo" style={{'backgroundImage' : 'url('  + props.it.photo + ')'}}>
            <div className="list-is-follower" title="You follow this Pro"></div>
        </div>
        <div className="is-subs"></div>
        <div className="pro-name">{props.it.name}</div>
        <div className="pro-follower">{props.it.count_followers} follower</div>

        <div className="list-title">About</div>

        <div className="title">Location</div>
        <div className="info">{props.it.city_name}, {props.it.state}</div>

        <div className="title">Activity</div>
        <div className="info">{props.it.activity_names}</div>

        <div className="title">Degrees and Sertifications</div>
        <div className="info">{props.it.degree_level}</div>

        <div className="title">Bio</div>
        <div className="info">{props.it.about}</div>

        <div>
            <div className="list-title">Videos</div>
            {
                <CarouselProvider
                visibleSlides={3}
                totalSlides={props.it.videos.length}
                step={3}
                naturalSlideWidth={90}
                naturalSlideHeight={90}
                key = {cnt++}
                > 
                <ButtonBack className="arrow-back"></ButtonBack>                   
                <Slider>
                {props.it.videos.map(it => (            
                <Slide key={cnt++}>
                    <div className="list-item trainer" style={{'backgroundImage' : 'url(' + it.image + ')'}} onClick={()=>onClickHandle(it)}>
                                                   
                        <div className="stream-info">
                        <div className="class-title">{it.title}</div>
                        </div>
                    </div>           
                </Slide>
                ))}
                </Slider>  
                <ButtonNext className="arrow-next"></ButtonNext>      
                </CarouselProvider>
            }
        </div>
    </div>
</div>)
 else res = (<Detail it={item} onClick={onBackHandle}/>);

return (
    res
)

}

export default TrainerDetails;
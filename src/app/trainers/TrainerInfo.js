import React from 'react';
import './TrainerInfo.css';


const TrainerInfo = (props) => {    
    return (<div className='user-wrap' onClick={props.onClick}>  
    <div className="user-image" style={{'backgroundImage' : 'url(' + props.it.photo + ')' }}>
    {props.it.is_follower === "1" ? <div className="list-is-follower" title= "You follow this Pro"></div>: null};
</div>
<div className="user-info">
<div className="header-line">
    <div className="user-is-live">
        { props.it.is_live === 1 ? 'Online' : 'Offline' }
    </div>
    <div className="user-viwers">{props.it.count_followers} followers</div>
</div>
<div className="content-line">
<div className="class-title">{props.it.name}</div>
    <div className="class-title">{props.it.activity_names}</div>
    <div className="class-title">{props.it.city_name}, {props.it.state}</div>
</div>
</div>
</div>)
    
}

export default TrainerInfo;
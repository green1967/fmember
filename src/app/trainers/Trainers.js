import React, {Component} from 'react';
//import './trainers.css';
import { connect } from 'react-redux';
import {Tabs, Tab} from 'react-bootstrap';
import TrainerInfo from './TrainerInfo';
import TrainerDetails from './TrainerDetails';
import {axios_i} from '../utils/utility';
import axios from 'axios';
import Spinner from '../UI/Spinner/Spinner';


class Trainers extends Component {

    state = {
        activityList: [],
        userList: [],
        selectedUserID: '',
        userInfo: {},
        loading: false,
    }

    componentDidMount() {
        this.setState({loading: true});
    const getUserList = () => axios_i.get('/users/get-pro', {headers: {
            'Authorization': 'Bearer ' + this.props.token}});
    

    const getActivityList = () => {
        return axios_i.get('/activities/', {headers: {
            'Authorization': 'Bearer ' + this.props.token}});
      }

      axios.all([getUserList(), getActivityList()])
      .then(axios.spread((users, activities) => {
//        console.log('trianers.users=', users.data);
//        console.log('trainers.activities=', activities.data);
        this.setState({userList: users.data, activityList: activities.data, loading: false});
      }))
      .catch( error => {
        this.setState({loading: false});
        console.log(error);                
        } );
    
    }

    onTrainerDetailHandle = (id) => {
//        console.log('pro.id=', id);
        this.setState({loading: true})
        axios_i.get('users/get-video', {headers: {
            'Authorization': 'Bearer ' + this.props.token}, params: {id: id}})
                .then( response => {
                if(response.status === 200)  {
 //                   console.log('trianers.get-pro=', response.data)
                    this.setState({selectedUserID: id, userInfo: response.data[0], loading: false});
      //               if(data.status==='OK') {
                    }                    
    //        });             
            } )
            .catch( error => {
                this.setState({loading: false});
                console.log(error);                
            } );

    }

    onBackHandle = () => {
        this.setState({selectedUserID: '', userInfo: {}});
    }

    render() {

    var list =null;
    var cnt=0;

    if(this.state.selectedUserID === '') list =  <Tabs>
        {this.state.activityList.map(item => (<Tab eventKey={item.id} key={cnt++} title={item.type_name}>
            {this.state.userList.map(itm => {
 //               console.log('item.id=', item.id, ' itm=', itm.activity_id, ' indexOF=', itm.activity_id.indexOf(item.id));
                if(itm.activity_id.indexOf(item.id) !== -1) return <TrainerInfo it={itm} key={itm.pid} onClick={()=>this.onTrainerDetailHandle(itm.pid)}/>; else return null;})}
        </Tab>))}				
</Tabs>
    else
      list = <TrainerDetails onBackClick={this.onBackHandle} it={this.state.userInfo}/>

// console.log('list=', list);
        return(
            <div className="container content-wrap">		
            <div className="class-list">
            {this.state.loading ? <Spinner/> : list}
            </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        user_id: state.user.user,
        token: state.user.token
    };
};

export default connect(mapStateToProps)(Trainers);

